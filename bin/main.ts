import { parseSong } from '../src/mod.ts'

const song = await Deno.readTextFile('./song4.root')

console.log(JSON.stringify(parseSong(song), null, 2))
