const containsLineBreak = (text: string) => {
  return /\n/.test(text)
}

export { containsLineBreak }
