const splitOnLineBreak = (text: string) => {
  return text.split(/\r?\n/)
}

export { splitOnLineBreak }
