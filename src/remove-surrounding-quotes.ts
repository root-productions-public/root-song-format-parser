const removeSurroundingQuotes = (text: string) => {
  return text.replace(/^"(.*)"$/, '$1')
}

export { removeSurroundingQuotes }
