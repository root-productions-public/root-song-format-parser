import { parseArrayFromString } from './parse-array-from-string.ts'

interface MetaData {
  key: string
  value: string[]
}

interface MetaDataResult {
  remainingText: string[]
  metaDataList: MetaData[]
  error: string
}

const parseMetaData = (text: string[]): MetaDataResult => {
  const metaDataList: MetaData[] = []

  for (let i = 0; i < text.length; i++) {
    const line = text[i]

    if (line.trim() === '---') {
      return {
        remainingText: text.slice(i + 1),
        metaDataList,
        error: '',
      }
    }

    const firstColonIndex = line.indexOf(':')

    if (firstColonIndex < 1) {
      return {
        remainingText: [],
        metaDataList,
        error: 'Was unable to parse meta data',
      }
    }

    const key = line.substring(0, firstColonIndex).trim()
    const value = line.substring(firstColonIndex + 1).trim()

    if ([key, value].some((x) => x.length < 1)) {
      return {
        remainingText: [],
        metaDataList,
        error: 'Was unable to parse meta data',
      }
    }

    metaDataList.push({
      key,
      value: parseArrayFromString(value),
    })
  }

  return {
    remainingText: [],
    metaDataList,
    error: 'Found no end of meta data',
  }
}

export { parseMetaData }
export type { MetaData, MetaDataResult }
