const parseLyricsRow = (text: string) => {
  const chordIndexes: number[] = []
  let lyrics = ''

  let index = 0

  for (const character of text) {
    if (character === '*') {
      chordIndexes.push(index)
    } else {
      lyrics += character
      index++
    }
  }

  return { chordIndexes, lyrics }
}

export { parseLyricsRow }
