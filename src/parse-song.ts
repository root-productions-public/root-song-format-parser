import { parseMetaData, type MetaData } from "./parse-meta-data.ts";
import { splitOnLineBreak } from "./split-on-line-break.ts";
import { parseSectionTitle } from "./parse-section-title.ts";
import { Measure, parseChordRow } from "./chord-row/mod.ts";
import { parseLyricsRow } from "./parse-lyrics-row.ts";

interface Row {
  measures: Measure[];
  lyrics: string[];
}

interface Block {
  title: string;
  rows: Row[];
}

const parseSong = (
  text: string,
  options?: {
    /** If metaData is not defined the text will considered to be a fill document with metaData and all will be parsed */
    metaData?: MetaData[] | undefined;
  },
) => {
  let body = splitOnLineBreak(text);
  let metaDataList = options?.metaData;

  if (metaDataList === undefined) {
    const { metaDataList: metaData, remainingText } = parseMetaData(body);

    body = remainingText;
    metaDataList = metaData;
  }

  const blocks: Block[] = [];

  let currentTitle = "";
  let rows: Row[] = [];
  let isInSection = false;

  for (const row of body) {
    const trimmedRow = row.trim();

    if (trimmedRow.at(0) === "[" && trimmedRow.at(-1) === "]") {
      // We have a section title
      currentTitle = parseSectionTitle(trimmedRow);
      isInSection = true;

      continue;
    }

    if (trimmedRow.at(0) === "|" && trimmedRow.at(-1) === "|") {
      rows.push({
        measures: parseChordRow(trimmedRow.substring(1, trimmedRow.length - 1)),
        lyrics: [],
      });

      continue;
    }

    if (trimmedRow.length < 1 && isInSection) {
      isInSection = false;
      blocks.push({
        rows: [...rows],
        title: currentTitle,
      });

      rows = [];
      currentTitle = "";

      continue;
    }

    const { lyrics, chordIndexes } = parseLyricsRow(row);

    if (lyrics.trim().length > 0) {
      const lastRow = rows[rows.length - 1];

      lastRow.lyrics = [...lastRow.lyrics, lyrics];

      let measureIndex = 0;
      let chordIndexInMeasure = 0;

      for (const chordIndex of chordIndexes) {
        // Assign chordIndex to the correct chord in the last row

        const chord =
          lastRow.measures[measureIndex]?.chords[chordIndexInMeasure] ?? null;

        if (chord) {
          lastRow.measures[measureIndex].chords[chordIndexInMeasure] = {
            ...chord,
            characterIndex: chordIndex,
          };
        }

        chordIndexInMeasure++;

        if (
          chordIndexInMeasure >= lastRow.measures[measureIndex].chords.length
        ) {
          // TODO: Handle errors where there are not the same amount of "stars" as chords
          chordIndexInMeasure = 0;
          measureIndex++;
        }
      }
    }
  }

  if (isInSection) {
    blocks.push({
      rows: [...rows],
      title: currentTitle,
    });
  }

  return {
    metaData: metaDataList,
    body: blocks,
  };
};

export { parseSong, type Row, type Block };
