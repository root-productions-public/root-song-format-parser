import { removeSurroundingQuotes } from './remove-surrounding-quotes.ts'
/*

		artist: Leion -> ['Leion']
		artist: [Leion, Wenger and Roos] -> ['Leion', 'Wenger and Roos']
		artist: ["Leion, Wenger and Roos"] -> ['Leion, Wenger and Roos']
		artist: ["Leion", "Wenger","Roos"] -> ['Leion', 'Wenger', 'Roos']	
		
		*/

const parseArrayFromString = (text: string) => {
  if (text.at(0) === '[' && text.at(-1) === ']') {
    const artists: string[] = []
    let isInQuote = false
    let currentWord = ''

    for (const character of text.substring(1, text.length - 1)) {
      if (character === '"') {
        isInQuote = !isInQuote

        continue
      }

      if (character === ',' && !isInQuote) {
        artists.push(currentWord.trim())
        currentWord = ''

        continue
      }

      currentWord = `${currentWord}${character}`
    }

    const currentWordTrimmed = currentWord.trim()

    if (currentWordTrimmed.length > 0) {
      artists.push(currentWordTrimmed)
    }

    return artists
  }

  return [removeSurroundingQuotes(text)]
}

export { parseArrayFromString }
