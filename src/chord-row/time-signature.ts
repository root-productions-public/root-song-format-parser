interface TimeSignature {
  /** Upper value of a time signature. ex: the 2 in 2/4 */
  beatCount: number
  /** Lower value of a time signature. ex: the 4 in 2/4 */
  beatUnit: number
}

export type { TimeSignature }
