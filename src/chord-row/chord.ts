interface Chord {
  name: string
  beatCount: number
  characterIndex: number
}

export type { Chord }
