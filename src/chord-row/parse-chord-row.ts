import { parseMeasure } from './parse-measure.ts'
import { type Chord } from './chord.ts'
import type { TimeSignature } from './time-signature.ts'

interface Measure {
  timeSignature: TimeSignature
  key: string
  chords: Chord[]
}

const parseChordRow = (
  text: string,
  previousTimeSignature: TimeSignature = { beatCount: 4, beatUnit: 4 }
) => {
  const measures: Measure[] = []

  const rowWithoutSurroundingPipes = text.substring(
    1,
    text.length - 1
  )

  for (const measure of rowWithoutSurroundingPipes.split('|')) {
    measures.push(parseMeasure(measure, previousTimeSignature))
  }

  return measures
}

export { parseChordRow }
export type { Measure }
