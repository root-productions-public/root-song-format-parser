import type { TimeSignature } from './time-signature.ts'
import { parseMeasureMetaData } from './parse-measure-meta-data.ts'
import { containsWhitespace } from '../contains-whitespace.ts'
import {
  BeatFraction,
  parseBeatFraction,
} from './parse-beat-fraction.ts'
import { giveChordsFractionOfMeasure } from './give-chords-fraction-of-measure.ts'
import type { Chord } from './chord.ts'

const parseMeasure = (
  text: string,
  previousTimeSignature: TimeSignature = { beatCount: 4, beatUnit: 4 }
) => {
  let isInMeasureMetaData = false
  let timeSignature = previousTimeSignature
  // TODO: Maybe we can just use currentSymbol for both
  let currentMetaData = ''
  let currentSymbol = ''

  const beatFractions: BeatFraction[][] = []

  const chords: Chord[] = []

  const pushSymbol = () => {
    const beatFraction = parseBeatFraction(currentSymbol)

    if (beatFraction) {
      beatFractions.at(-1)?.push(beatFraction)
    } else {
      chords.push({
        name: currentSymbol.trim(),
        beatCount: 1,
        characterIndex: -1,
      })
      beatFractions.push([])
    }

    currentSymbol = ''
  }

  for (const character of text) {
    if (character === '[') {
      // This means we are starting to parse measure meta data
      isInMeasureMetaData = true
      continue
    }

    if (character === ']') {
      // This means we should be done parsing measure meta data
      isInMeasureMetaData = false

      const { error, timeSignature: newTimeSignature } =
        parseMeasureMetaData(currentMetaData)

      if (error) {
        // TODO: Handle error
        console.error(error)
      }

      if (newTimeSignature) {
        timeSignature = newTimeSignature
      }

      continue
    }

    if (isInMeasureMetaData) {
      // Adding to measure meta data
      currentMetaData += character
    } else {
      // Should be parsing chords or their fractions of the measure
      if (
        containsWhitespace(character) &&
        currentSymbol.trim().length > 0
      ) {
        pushSymbol()

        continue
      }

      currentSymbol += character
    }
  }

  if (currentSymbol.trim().length > 0) {
    pushSymbol()
  }

  return {
    timeSignature,
    chords: giveChordsFractionOfMeasure(
      timeSignature,
      beatFractions,
      chords
    ),
    key: 'G',
  }
}

export { parseMeasure }
export type { Chord }
