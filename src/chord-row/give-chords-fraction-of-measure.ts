import type { Chord } from './chord.ts'
import {
  BeatFraction,
  beatFractionToNumber,
} from './parse-beat-fraction.ts'
import { TimeSignature } from './time-signature.ts'

/*

| Em D . C | 
-> Em 2/4, D 1/4, C 1/4
-> Em 1/4, D 1/4, C 2/4

| Em D C . | 
-> Em 2/4, D 1/4, C 1/4
-> Em 1/4, D 2/4, C 1/4

| Em . D C | 
-> Em 1/4, D 2/4, C 1/4
-> Em 1/4, D 1/4, C 2/4

| Em . D C . | 
-> Em 1/4, D 2/4, C 1/4

*/

const giveChordsFractionOfMeasure = (
  timeSignature: TimeSignature,
  beatFractions: BeatFraction[][],
  chords: Chord[]
): Chord[] => {
  if (beatFractions.length !== chords.length) {
    console.error(
      'The beat fractions array and the chords array must be of the same length'
    )
    return []
  }

  let timeNotInUse = timeSignature.beatCount
  const unspecifiedChordIndexes = []
  const newChords: Chord[] = []

  for (let i = 0; i < chords.length; i++) {
    if (beatFractions[i].length < 1) {
      unspecifiedChordIndexes.push(i)
      newChords.push(chords[i])
    } else {
      const beatsForChord =
        beatFractions[i]
          .map((beatFraction) => beatFractionToNumber(beatFraction))
          .reduce(
            (previous, current) => (previous ?? 0) + (current ?? 0),
            0
          ) ?? 0

      timeNotInUse = timeNotInUse - beatsForChord

      newChords.push({
        ...chords[i],
        beatCount: beatsForChord,
      })
    }
  }

  // TODO: Fill the remaining chords

  for (let i = 0; i < unspecifiedChordIndexes.length; i++) {
    const amountOfRemainingChords = unspecifiedChordIndexes.length - i

    const beatsForChord =
      amountOfRemainingChords < 2
        ? timeNotInUse / amountOfRemainingChords
        : Math.ceil(timeNotInUse / amountOfRemainingChords)

    timeNotInUse = timeNotInUse - beatsForChord

    newChords[unspecifiedChordIndexes[i]] = {
      ...newChords[unspecifiedChordIndexes[i]],
      beatCount: beatsForChord,
    }
  }

  return newChords
}

export { giveChordsFractionOfMeasure }
