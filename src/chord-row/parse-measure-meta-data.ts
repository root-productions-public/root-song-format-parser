import { TimeSignature } from './time-signature.ts'

const parseMeasureMetaData = (text: string) => {
  let timeSignature: TimeSignature | null = null

  for (const metaData of text
    .split(',')
    .map((x) => x.trim())
    .filter((x) => x.length > 0)) {
    if (!isNaN(Number.parseInt(metaData[0]))) {
      const [beatCount = '0', beatUnit = '0'] = metaData.split('/')

      const [beatCountAsNumber, beatUnitAsNumber] = [
        beatCount,
        beatUnit,
      ].map((x) => Number.parseInt(x))

      if (
        [beatCountAsNumber, beatUnitAsNumber].some(
          (x) => isNaN(x) && x < 1
        )
      ) {
        return {
          timeSignature: null,
          error: 'Unable to parse time signature',
        }
      }

      timeSignature = {
        beatCount: beatCountAsNumber,
        beatUnit: beatUnitAsNumber,
      }
    }
  }

  return {
    timeSignature,
    error: '',
  }
}

export { parseMeasureMetaData }
