enum BeatFraction {
  Full = '.',
  Half = ',',
  Fourth = ':',
  Eighth = ';',
  Sixteenth = '-',
  ThirtySecond = '_',
  SixtyFourth = '^',
}

const parseBeatFraction = (text: string): BeatFraction | null => {
  switch (text) {
    case BeatFraction.Full:
      return BeatFraction.Full
    case BeatFraction.Half:
      return BeatFraction.Half
    case BeatFraction.Fourth:
      return BeatFraction.Fourth
    case BeatFraction.Eighth:
      return BeatFraction.Eighth
    case BeatFraction.Sixteenth:
      return BeatFraction.Sixteenth
    case BeatFraction.ThirtySecond:
      return BeatFraction.ThirtySecond
    case BeatFraction.SixtyFourth:
      return BeatFraction.SixtyFourth
    default:
      return null
  }
}

const beatFractionNumberMap = new Map([
  [BeatFraction.Full, 1],
  [BeatFraction.Half, 1 / 2],
  [BeatFraction.Fourth, 1 / 4],
  [BeatFraction.Eighth, 1 / 8],
  [BeatFraction.Sixteenth, 1 / 16],
  [BeatFraction.ThirtySecond, 1 / 32],
  [BeatFraction.SixtyFourth, 1 / 64],
])

const beatFractionToNumber = (beatFraction: BeatFraction) =>
  beatFractionNumberMap.get(beatFraction) ?? null

export { BeatFraction, parseBeatFraction, beatFractionToNumber }
