const containsWhitespace = (text: string) => {
  return /\s/.test(text)
}

export { containsWhitespace }
