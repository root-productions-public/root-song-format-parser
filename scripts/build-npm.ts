import { build, emptyDir } from 'https://deno.land/x/dnt/mod.ts'

await emptyDir('./npm')

await build({
  entryPoints: ['./src/mod.ts'],
  outDir: './npm',
  shims: {
    // see JS docs for overview and more options
    deno: true,
  },
  package: {
    // package.json properties
    name: '@roots-productions/root-song-format-parser',

    version: Deno.args[0],
    description: 'A library for parsing the root song format',
    license: 'MIT',
    repository: {
      type: 'git',
      url: 'git+https://gitlab.com/root-productions-public/root-song-format-parser.git',
    },
  },
})

Deno.copyFileSync('readme.md', 'npm/readme.md')
